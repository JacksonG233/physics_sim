self.collision_c = []
        # placing each corner in a half before searching for specific possible collision tile "this.k"
        for self.i in range(len(self.limbs)):
            for self.j in range(4):
                if self.limbs[self.i].corners[self.j][0] < x[0][math.floor(len(x[0]) / 2)]:
                    self.k = 0
                    while x[0][self.k] < self.limbs[self.i].corners[self.j][0]:
                        self.k += 1

                else:
                    self.k = math.floor(len(x[0]) / 2)
                    while x[0][self.k] < self.limbs[self.i].corners[self.j][0]:
                        self.k += 1


                # abbreviating
                self.a = self.limbs[self.i].corners[self.j][0]
                self.b = self.limbs[self.i].corners[self.j][1]
                self.corner_angle = (self.b - x[1][self.k]) / (self.a - x[0][self.k])
                if tile_angle[self.k - 1] > self.corner_angle:
                    self.collision_c.append([self.i, self.j])
                    self.d = dist((self.a, self.b), (x[0][self.k], x[1][self.k]))
                    self.e = dist((self.a, self.b), (x[0][self.k - 1], x[1][self.k - 1]))
                    self.f = dist((x[0][self.k], x[1][self.k]), (x[0][self.k - 1], x[1][self.k - 1]))
                    self.s = (self.d + self.e + self.f) / 2
                    self.area = math.sqrt(self.s * (self.s - self.d) * (self.s - self.e) * (self.s - self.f))
                    self.heigh = round((self.area / self.f) * 20) / 10
                    self.y2 = self.heigh * (1 / math.sqrt(1 + (tile_angle[self.k - 1]) ** 2))
                    self.x2 = self.heigh * (tile_angle[self.k - 1] / math.sqrt(1 + (tile_angle[self.k - 1]) ** 2))

                    if tile_angle[self.k - 1] > 0:
                        self.center_mass[0] = self.center_mass[0] - self.x2
                        self.pos_x = self.pos_x - self.x2

                    else:
                        self.center_mass[0] = self.center_mass[0] + self.x2
                        self.pos_x = self.pos_x + self.x2

                    self.center_mass[1] = self.center_mass[1] - self.y2
                    self.pos_y = self.pos_y - self.y2
                    for self.l in range(len(self.limbs)):
                        self.limbs[self.l].find_corners(jumpers)

                    self.hyp = math.sqrt((self.velocityX) ** 2 + self.velocityY ** 2)
                    self.velocityAngle = math.asin(self.velocityY / self.hyp)
                    self.tile_angle = math.atan(tile_angle[self.k - 1])
                    self.newVel = math.sin((math.pi / 2) - (self.velocityAngle + self.tile_angle)) * self.hyp
                    # print(math.degrees(self.velocityAngle))
                    # print(math.degrees(self.tile_angle))
                    # print(math.degrees(self.tile_angle + math.pi / 2))
                    # print(self.velocityY)
                    # print(self.newVel)
                    # print(math.cos(self.tile_angle))
                    # print(math.sin(self.tile_angle))
                    # print("/////////////////////")
                    if self.forceAngle > self.tile_angle + math.pi / 2:
                        self.velocityX = -abs(math.cos(self.tile_angle) * self.newVel)

                        if self.tile_angle > 0:
                            self.velocityY = abs(math.sin(self.tile_angle) * self.newVel)

                        else:
                            self.velocityY = -abs(math.sin(self.tile_angle) * self.newVel)

                    else:
                        self.velocityX = abs(math.cos(self.tile_angle) * self.newVel)
                        if self.tile_angle > 0:
                            self.velocityY = -abs(math.sin(self.tile_angle) * self.newVel)
                        else:
                            self.velocityY = abs(math.sin(self.tile_angle) * self.newVel)