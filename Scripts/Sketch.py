import pygame
from jumpers import Jumper
from functions import *
import sys
sys.path.insert(0, '/Users/jackson/Documents/Python/Modules')
import collisions

pygame.init()

running = True
jumpers = []
maxLimbs = 4
hingeSpeed = 3
pause = False
frame = False
tiles = []
tileX = [0, 325, 975, 1300]
tileY = [525, 525, 575, 575]
tileMover = False
hold_node = -1
canvasX = 1300
canvasY = 700
tile_info = [tileX, tileY, canvasY]

screen = pygame.display.set_mode([canvasX, canvasY])

# def preload ():
#    panda = loadImage('images/panda.jpg')
#    image(panda, 0, 0)


# creating single jumper
jumpers.append(Jumper(650, 200, hingeSpeed, screen))
jumpers[0].num = 0
jumpers[0].create(0, jumpers)
# drawing floor


tile_draw(screen, tile_info)
tile_angle = find_angles(tile_info)
tile_info = [tileX, tileY, canvasY, tile_angle, canvasX]


while running:
    pygame.display.update()

    if not pause:
        frame = True
    if frame:
        screen.fill((240, 240, 240))

        if tileMover:
            new = tile_move(hold_node, tile_info, screen)
            hold_node = new[0]
            tileX = new[1]
            tileY = new[2]
            tile_angle = find_angles(tile_info)
            tile_info[3] = tile_angle
            tile_draw(screen, tile_info)
            jumpers[0].render()
        else:
            tile_draw(screen, tile_info)
            jumpers[0].hinge()
            jumpers[0].rot_center(jumpers)
            jumpers[0].sum_forces()
            jumpers[0].collision(tile_info, screen)
            jumpers[0].render()
            frame = False

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:

            if event.key == pygame.K_k:
                keys = pygame.key.get_pressed()
                i = 0
                while keys[i] == 0:
                    i += 1
                print(i)

            if event.key == pygame.K_p:
                jumpers[0].print = True

            if event.key == pygame.K_t:
                True

            if event.key == pygame.K_q:
                running = False

            if event.key == pygame.K_f:
                if tileMover:
                    tileMover = False
                else:
                    tileMover = True

            if event.key == pygame.K_RIGHT:
                jumpers[0].turn += 1

            if event.key == pygame.K_LEFT:
                jumpers[0].turn -= 1

            if event.key == pygame.K_1:
                jumpers[0].hinge_count[0] -= 1

            if event.key == pygame.K_2:
                jumpers[0].hinge_count[0] += 1

            if event.key == pygame.K_3:
                jumpers[0].hinge_count[1] -= 1

            if event.key == pygame.K_4:
                jumpers[0].hinge_count[1] += 1

        if event.type == pygame.KEYUP:

            if event.key == pygame.K_RIGHT:
                jumpers[0].turn -= 1

            if event.key == pygame.K_LEFT:
                jumpers[0].turn += 1

            if event.key == pygame.K_1:
                jumpers[0].hinge_count[0] += 1

            if event.key == pygame.K_2:
                jumpers[0].hinge_count[0] -= 1

            if event.key == pygame.K_3:
                jumpers[0].hinge_count[1] += 1

            if event.key == pygame.K_4:
                jumpers[0].hinge_count[1] -= 1
