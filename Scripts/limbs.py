import pygame
import math


class Limb:
    def __init__(self, a, b, c, d, x, y):
        self.jumperNum = x
        self.arrayNum = y
        self.length = a
        self.height = b
        self.weight = a * b
        self.angle = d
        self.anchor = c
        # anchor is the corner of the previous Limb to attach to
        # for first limb set -1
        self.corners = [0, 0, 0, 0]
        self.center_mass = [0, 0]
        self.short = 0
        self.point_list = []
        # creating corners ghost array
        for self.i in range(4):
            self.corners[self.i] = [0, 0]

    def find_corners(self, this):

        # assigning "corner 0" coordinates of the remaining limbs using specified
        # anchor point on the previous limb
        self.corners[0] = [this.limbs[self.arrayNum - 1].corners[self.anchor][0],
                           this.limbs[self.arrayNum - 1].corners[self.anchor][1]]

        # assigning "corner 0" coordinates of the first limb from jumper position
        if self.arrayNum == 0:
            self.corners[0] = [this.pos_x, this.pos_y]

        # calculating remaining corners of all limbs from specified angle, length, and height
        self.short = (math.radians(self.angle + this.angle))

        self.corners[1] = [self.corners[0][0] + math.cos(self.short) * self.length,
                           self.corners[0][1] + math.sin(self.short) * self.length]
        self.corners[2] = [self.corners[1][0] - math.sin(self.short) * self.height,
                           self.corners[1][1] + math.cos(self.short) * self.height]
        self.corners[3] = [self.corners[0][0] - math.sin(self.short) * self.height,
                           self.corners[0][1] + math.cos(self.short) * self.height]

    def render(self, screen):
        # sending info for drawing limb
        self.point_list = [(self.corners[0][0], self.corners[0][1]), (self.corners[1][0], self.corners[1][1]),
                           (self.corners[2][0], self.corners[2][1]), (self.corners[3][0], self.corners[3][1])]
        pygame.draw.polygon(screen, (50, 50, 50), self.point_list)
