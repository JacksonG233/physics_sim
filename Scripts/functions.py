import pygame
import sys
sys.path.insert(0, '/Users/jackson/Documents/Python/Modules')
import collisions


def tile_draw(screen, x):

    # pulling values from array to draw tile segments

    for i in range(len(x[0]) - 1):

        point_list = [(x[0][i], x[1][i]), (x[0][i + 1], x[1][i + 1]),
                      (x[0][i + 1], x[2]), (x[0][i], x[2])]
        pygame.draw.polygon(screen, (50, 50, 50), point_list)


def tile_move(hold_node, x, screen):
    # moving the ground nodes around
    # all .01's are used to avoid < 0 or > 0 problems
    # testing to see if currently dragging a node to avoid loss from speed
    keys = pygame.key.get_pressed()
    mouse = pygame.mouse.get_pressed()
    mouse_pos = pygame.mouse.get_pos()

    if hold_node > -1:

        if mouse[0] == 1:

            if not (keys[306] and hold_node > 0):

                # switching held node if dragged past another
                while mouse_pos[0] < x[0][hold_node - 1] + 1:
                    save = x[1][hold_node]
                    x[1][hold_node] = x[1][hold_node - 1]
                    x[1][hold_node - 1] = save
                    x[0][hold_node] = x[0][hold_node - 1]
                    hold_node -= 1

                while mouse_pos[0] > x[0][hold_node + 1] - 1:
                    save = x[1][hold_node]
                    x[1][hold_node] = x[1][hold_node + 1]
                    x[1][hold_node + 1] = save
                    x[0][hold_node] = x[0][hold_node + 1]
                    hold_node += 1

            x[0][hold_node] = mouse_pos[0]
            x[1][hold_node] = mouse_pos[1]

            # Keeping tile flat when holding shift
            if keys[107] and hold_node > 0:
                x[1][hold_node] = x[1][hold_node - 1]

            # keeping tile straight when holding ctrl
            if keys[306] and hold_node > 0:
                if x[0][hold_node] < x[0][hold_node - 1]:
                    x[0][hold_node] = x[0][hold_node - 1] + .5

                if x[0][hold_node] > x[0][hold_node + 1]:
                    x[0][hold_node] = x[0][hold_node + 1] - .5

            # keeping nodes inside parameters
            if hold_node == 0:
                x[0][hold_node] = 0.01

            if hold_node == len(x[0]) - 1:
                x[0][hold_node] = x[4] - .01

            if x[1][hold_node] < 0:
                x[1][hold_node] = 0.01

            if x[1][hold_node] > x[2]:
                x[1][hold_node] = x[2] - .01

            pygame.draw.circle(screen, (100, 100, 100, 200), (x[0][hold_node], x[1][hold_node]), 15)
            
        else:
            # canceling hold if mouse is released
            pygame.draw.circle(screen, (100, 100, 100, 200), (x[0][hold_node], x[1][hold_node]), 15)
            hold_node = -1

    else:
        # testing for tile corner near mouse
        for i in range(len(x[0])):
            if abs(mouse_pos[0] - x[0][i]) < 20 and abs(mouse_pos[1] - x[1][i]) < 20:
                # using correct color and moving tile corner to mouse

                color = (100, 100, 100, 200)

                # testing for click and starting drag
                if mouse[0] == 1:
                    x[0][i] = mouse_pos[0]
                    x[1][i] = mouse_pos[1]
                    # keeping first node left
                    if i == 0:
                        x[0][i] = 0

                    # keeping last node right
                    if i == len(x[0]) - 1:
                        x[0][i] = x[4]

                    # saving node number for speed loss avoidance
                    hold_node = i

                # deleting nodes
                elif keys[8] == 1:
                    x[0].remove(x[0][i])
                    x[1].remove(x[1][i])
                    break

                else:
                    # canceling hold if no click
                    hold_node = -1

            else:
                color = (200, 200, 200, 150)

            # making sure node is inside parameters
            if x[2] > x[1][i] > 0 and x[4] + .01 > x[0][i] > -0.01:
                pygame.draw.circle(screen, color, (x[0][i], x[1][i]), 15)

        # creating new node if there is no current highlight or hold
        if hold_node == -1:
            if mouse[0] == 1:
                # making sure node is inside parameters
                if x[2] > mouse_pos[1] > 0 and x[4] > mouse_pos[0] > 0:
                    j = 0
                    # finding correct array placement
                    while x[0][j] < mouse_pos[0]:
                        j += 1

                    x[0].insert(j, mouse_pos[0])
                    x[1].insert(j, mouse_pos[1])
                    hold_node = j
    return [hold_node, x[0], x[1]]


def find_angles(x):

    # making new array of slopes for each of the tiles
    tile_angle = []
    for i in range(len(x[0]) - 1):
        zero_error = (x[0][i + 1] - x[0][i])
        if zero_error == 0:
            zero_error = .000001
        tile_angle.append((x[1][i + 1] - x[1][i]) / zero_error)
    return tile_angle


def center_o_mass(x, y, jumpers, rectify):

    # saving previous center of mass

    center_mass1 = x.copy()
    mass_centers = []

    for i in range(len(jumpers[y].limbs)):

        # abbreviating

        a = jumpers[y].limbs[i].corners[0][0]
        b = jumpers[y].limbs[i].corners[0][1]
        c = jumpers[y].limbs[i].corners[2][0]
        d = jumpers[y].limbs[i].corners[2][1]
        e = jumpers[y].limbs[i].corners[1][0]
        f = jumpers[y].limbs[i].corners[1][1]
        g = jumpers[y].limbs[i].corners[3][0]
        h = jumpers[y].limbs[i].corners[3][1]

        # finding the center of each limb by intersecting two lines drawn
        #  diagonally across the limb from opposing corners

        intersection = collisions.line_line(a, b, c, d, e, f, g, h, True)
        jumpers[y].limbs[i].center_mass = [intersection[0], intersection[1]]
        mass_centers.append([intersection[0], intersection[1], jumpers[y].limbs[i].weight])

        # copying limb 0 & 1 info into "pointA" & "pointB"

    point_a = mass_centers[0].copy()
    point_b = mass_centers[1].copy()

    # finding the over all center of mass by starting with limbs 0 & 1 (A & B) and
    # copying their combined center of mass into A before assigning
    # the next limb to B and repeating

    for i in range(1, len(mass_centers)):
        weight_ratio = point_a[2] / (point_a[2] + point_b[2])
        point_a[0] = point_b[0] + weight_ratio * (point_a[0] - point_b[0])
        point_a[1] = point_b[1] + weight_ratio * (point_a[1] - point_b[1])
        point_a[2] = point_a[2] + point_b[2]
        if i < len(mass_centers) - 1:
            point_b = [mass_centers[i + 1][0], mass_centers[i + 1][1], mass_centers[i + 1][2]]
        else:
            x = [point_a[0], point_a[1], point_a[2]]

    if rectify:
        # finding difference in center of mass and changing
        # jumper position accordingly
        if x[0] != center_mass1[0] or x[1] != center_mass1[1]:
            # indicator for first frame to avoid changing center of mass
            if center_mass1[0]:
                jumpers[y].pos_x = jumpers[y].pos_x + (center_mass1[0] - x[0])
                jumpers[y].pos_y = jumpers[y].pos_y + (center_mass1[1] - x[1])
                x = center_mass1.copy()

        return x
    else:
        # returning (x coord, y coord, weight)
        return x
