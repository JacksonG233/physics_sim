import pygame
import math
from limbs import Limb
from functions import *
import collisions


class Jumper:
    
    def __init__(self, x, y, z, screen):
        self.screen = screen
        self.gravity = [True, 0.2]
        self.num = 0
        self.pos_x = x
        self.pos_y = y
        self.limbs = []
        self.angle = 0
        self.center_mass = [0, 0]
        self.pointA = [0, 0]
        self.pointB = [0, 0]
        self.turn = 0
        self.collision_c = []
        self.onGround = False
        self.balanced = False
        self.velocityX = 0
        self.velocityY = 0
        self.totalForce = [0, 0]
        self.rot_speed = z
        self.rotAcc = 0
        self.hinge_count = []
        self.hinge_active = []
        self.counter = 0
        self.t_weight = 0
        self.b_weight = 0
        self.j = 0
        self.k = 0
        self.hyp = 0
        self.forceAngle = 0
        self.components = 0
        self.point = 0
        self.line1 = 0
        self.line2 = 0
        self.print = False

        for self.i in range(30):
            self.hinge_count.append(0)

    def create(self, x, jumpers):
        # adding limbs to jumper
        # append(this.limbs, new limb(50, 50, -1, 0, x, 0))
        # append(this.limbs, new limb(50, 50, 3, 90, x, 1))
        # append(this.limbs, new limb(50, 50, 1, 0, x, 2))

        self.limbs.append(Limb(50, 35, -1, 0, x, 0))
        self.limbs.append(Limb(50, 15, 3, 90, x, 1))
        self.limbs.append(Limb(37, 20, 1, 0, x, 2))
        self.rot_center(jumpers)
        self.center_mass = center_o_mass(self.center_mass, self.num, jumpers, False)

    def render(self):

        # rendering individual limbs

        for self.i in range(len(self.limbs)):
            self.limbs[self.i].render(self.screen)

        # drawing point on center of mass

        pygame.draw.rect(self.screen, (255, 255, 255), (self.center_mass[0] - 2, self.center_mass[1] - 2, 5, 5))
        pygame.draw.rect(self.screen, (0, 0, 0), (self.center_mass[0] - 1, self.center_mass[1] - 1, 3, 3))
        pygame.draw.rect(self.screen, (255, 255, 255), (self.center_mass[0], self.center_mass[1], 1, 1))

    def hinge(self):

        # looking at which hinges are active in which directions from key presses
        # counting through the hinges then adding to list of actives

        self.hinge_active = []

        for self.i in range(len(self.limbs) - 1):

            if self.hinge_count[self.i] > 0:
                self.hinge_active.append(self.i)

            if self.hinge_count[self.i] < 0:
                self.hinge_active.append(self.i + 0.1)

        # adding up and then dividing weights on either side of the hinge to
        # determine rotation ratio

        for self.i in range(len(self.hinge_active)):
            self.t_weight = 0
            self.b_weight = 0
            self.j = 0

            while self.hinge_active[self.i] + 0.1 > self.j:
                self.t_weight = self.t_weight + self.limbs[self.j].weight
                self.j += 1

            while self.j < len(self.limbs):
                self.b_weight = self.b_weight + self.limbs[self.j].weight
                self.j += 1

            self.t_weight = self.t_weight / (self.t_weight + self.b_weight)
            self.b_weight = 1 - self.t_weight

            # checking hinge movement direction
            # top counter clockwise movement is indicated by an added decimal

            if self.hinge_active[self.i] - round(self.hinge_active[self.i]) == 0:

                # changing angles according to rotation ratios      A  =  F / M

                self.j = 0

                while self.hinge_active[self.i] + 0.1 > self.j:
                    self.limbs[self.j].angle = self.limbs[self.j].angle + self.b_weight * self.rot_speed
                    self.j += 1

                while self.j < len(self.limbs):
                    self.limbs[self.j].angle = self.limbs[self.j].angle - self.t_weight * self.rot_speed
                    self.j += 1

            else:

                self.j = 0

                while self.hinge_active[self.i] + 0.1 > self.j:
                    self.limbs[self.j].angle = self.limbs[self.j].angle - self.b_weight * self.rot_speed
                    self.j += 1

                while self.j < len(self.limbs):
                    self.limbs[self.j].angle = self.limbs[self.j].angle + self.t_weight * self.rot_speed
                    self.j += 1

    def rot_center(self, jumpers):
        
        if self.turn > 0:
            self.angle += 1

        if self.turn < 0:
            self.angle -= 1

        for self.i in range(len(self.limbs)):
            self.limbs[self.i].find_corners(self)

        self.center_mass = center_o_mass(self.center_mass, self.num, jumpers, True)

    def sum_forces(self):
    
        # gravity

        if self.gravity[0]:
            self.totalForce[1] = self.totalForce[1] + self.gravity[1]
            self.velocityY = self.velocityY + self.gravity[1]
            
        self.hyp = math.sqrt(self.totalForce[0] ** 2 + self.totalForce[1] ** 2)
        if self.hyp == 0:
            self.hyp = .000001
        self.forceAngle = math.asin(self.totalForce[1] / self.hyp)
        self.center_mass[1] = self.center_mass[1] + self.velocityY
        self.center_mass[0] = self.center_mass[0] + self.velocityX
        self.pos_y = self.pos_y + self.velocityY
        self.pos_x = self.pos_x + self.velocityX
        
        for self.i in range(len(self.limbs)):
            self.limbs[self.i].find_corners(self)

    def collision(self, x, screen):
        # if self.gravity[0]:
        for self.i in range(len(self.limbs)):
            for self.j in range(4):
                # finding the floor tile to test each point against
                self.k = 1
                while self.limbs[self.i].corners[self.j][0] > x[0][self.k]:
                    self.k += 1
                # shortening and testing if corner is below tile line
                self.point = self.limbs[self.i].corners[self.j]
                self.line1 = [x[0][self.k - 1], x[1][self.k - 1]]
                self.line2 = [x[0][self.k], x[1][self.k]]
                pygame.draw.circle(screen, (255, 0, 0), (int(self.point[0]), int(self.point[1])), 2)
                collide = collisions.point_below(self.point, self.line1, self.line2)
                if collide[0]:
                    if self.print:
                        print('Limb:', self.i, ' Corner:', self.j, ' x coordinate- ',
                              self.limbs[self.i].corners[self.j][0])
                        print('Tile:', self.k, ' x coordinate- ', x[0][self.k])
                    # if corner is below finding x and y components and adjusting
                    self.components = self.find_components(self.point, self.line1, self.line2, x, self.k, self.print)
                    self.print = False
                    self.center_mass[0] += self.components[0]
                    self.center_mass[1] += self.components[1]
                    self.pos_x += self.components[0]
                    self.pos_y += self.components[1]
                    for self.l in range(len(self.limbs)):
                        self.limbs[self.l].find_corners(self)
                    self.velocityY = 0
                    # self.gravity[0] = False

    @staticmethod
    def find_components(point, line1, line2, x, k, pprint):
        # taking the x coord of the corner and finding the corresponding y coord
        # on the tile and testing it against the y coord of the corner
        dist = collisions.point_line_dist(point, line1, line2)
        slope = x[3][k - 1]

        if slope == 0:
            return [0, -dist]
        elif slope > 0:
            return [dist * math.sin(math.atan(slope)), -dist * math.cos(math.atan(slope))]
        else:
            return [dist * math.sin(math.atan(slope)), -dist * math.cos(math.atan(slope))]

    # def momentum(self):
